from pymongo import MongoClient

class DB(object):
    def __init__(self, dbName):
        self.isConnect = 1
        try:
            # MongoHQ db server
            raise
        except:
            try:
                # local db server
                client = MongoClient('localhost', 27017)
            except:
                isConnect = 0
        try:
            db       = client[dbName]
            self.col = db.User
        except:
            pass

    def list(self):
        return self.col.find()
