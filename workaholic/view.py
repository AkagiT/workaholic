from flask import render_template, request
from users import User
from workaholic import app, __version__

@app.route("/")
def hello():
    return render_template('menu.html', ver=__version__)

@app.route("/tasks")
def list_task():
    return "Hello "+__name__

@app.route("/shots")
def list_shot():
    return "Hello "+__name__

@app.route("/assets")
def list_asset():
    return "Hello "+__name__

@app.route("/users")
def list_user():
    user = User("UserDB")
    if not user.isConnect:
        return render_template('error.html', subject="Connection Error!", description='failed to connect db.')
    ul = user.list()
    return render_template('listUsers.html', users=ul)

@app.route("/users/create", methods=['GET', 'POST'])
def add_user():
    if request.method == 'POST':
        user = User("UserDB")
        if not user.isConnect:
            return render_template('error.html', subject="Connection Error!", description='failed to connect db.')
        user.add( username = request.form['username'],
                  passwd   = request.form['passwd'],
                  email    = request.form['email'])
        return render_template('addUser.html', created=1)
    return render_template('addUser.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)
