from flask import Flask

__version__='0.0.1'
__bootstrap_version__ = '3.3.5'
__jquery_version__    = '2.1.4'

app = Flask(__name__)
app.debug = True

from workaholic import view
